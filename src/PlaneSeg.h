/*
 * Copyright 2018 Pedro Proenza <p.proenca@surrey.ac.uk> (University of Surrey)
 *
 */

#pragma once
#include <iostream>
#include "Params.h"
#include <Eigen/Dense>
#include <ctime>

using namespace std;

class PlaneSeg
{
  public:
		PlaneSeg(const Eigen::MatrixXf& cloud_array, int cell_id,
			 int nr_pts_per_cell, int cell_width)		;

    void	fitPlane()						;
    void	expandSegment(const PlaneSeg* plane_seg)		;
    void	clearPoints();

  public:
    float	score;
    float	MSE;
    bool	planar;
    
  // Plane params
    double	mean[3];
    double	normal[3];
    double	d;

  private:
    int		_nr_pts, _min_nr_pts;
    double	_x_acc, _y_acc, _z_acc,
		_xx_acc, _yy_acc, _zz_acc,
		_xy_acc, _xz_acc, _yz_acc;
};

