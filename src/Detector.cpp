/*!
* \file		Detector.cpp
* \author	Toshio UESHIBA
*/
#include "Detector.h"
#include <aist_utility/sensor_msgs.h>
#include <aist_utility/opencv.h>

namespace cape_ros
{
/************************************************************************
*  class Detector							*
************************************************************************/
Detector::Detector(const ros::NodeHandle& nh)
    :_nh(nh),
     _it(_nh),
     _camera_sub(_it.subscribeCamera("/depth", 1, &Detector::camera_cb, this)),
     _cloud_sub(_nh.subscribe<cloud_t>("/pointcloud", 1,
				       boost::bind(&Detector::cloud_cb,
						   this, _1))),
     _image_pub(_it.advertise("image", 1)),
     _cloud(),
     _cell_map(),
     _organized_cloud(),
     _label(),
     _cape(nullptr),
     _ddr(_nh),
     _patch_size(20),
     _detect_cylinder(true),
     _cos_angle_max(std::cos(M_PI/12)),
     _max_merge_dist(50.0)
{
  // Set ddr parameters
    _ddr.registerVariable<int>(
	"patch_size", _patch_size,
	boost::bind(&Detector::set_cape_parameter<int>,
		    this, &Detector::_patch_size, _1),
	"Patch size for CAPE", 5, 30);
    _ddr.registerVariable<bool>(
	"detect_cylinder", _detect_cylinder,
	boost::bind(&Detector::set_cape_parameter<bool>,
		    this, &Detector::_detect_cylinder, _1),
	"Turn on/off cylinder detection", false, true);
    _ddr.registerVariable<double>(
	"cos_angle_max", _cos_angle_max,
	boost::bind(&Detector::set_cape_parameter<double>,
		    this, &Detector::_cos_angle_max, _1),
	"Max angular difference", std::cos(M_PI/6), std::cos(M_PI/36));
    _ddr.registerVariable<double>(
	"max_merge_dist", _max_merge_dist,
	boost::bind(&Detector::set_cape_parameter<double>,
		    this, &Detector::_max_merge_dist, _1),
	"Max merge distance", 20.0, 100.0);
    _ddr.publishServicesTopics();

  // Setup colormap
    for(int i = 0; i < 100; i++)
        _colormap.emplace_back(rand() % 255, rand() % 255, rand() % 255);

  // Add specific colors for planes
    _colormap[0] = {0, 0, 255};
    _colormap[1] = {255, 0, 204};
    _colormap[2] = {255, 100, 0};
    _colormap[3] = {0, 153, 255};

  // Add specific colors for cylinders
    _colormap[50] = {178, 255, 0};
    _colormap[51] = {255, 0, 51};
    _colormap[52] = {0, 255, 51};
    _colormap[53] = {153, 0, 255};
}

void
Detector::run()
{
    ros::spin();
}

void
Detector::camera_cb(const image_p& depth_msg,
		    const camera_info_p& camera_info_msg)
{
    try
    {
	using namespace aist_utility;

	set_sizes(depth_msg->height, depth_msg->width);
	
	depth_to_points<float>(*camera_info_msg, *depth_msg, _cloud.begin(),
			       meters<float>);
	organize_cloud();
	_label = 0;
	
	int			nplanes, ncylinders;
        vector<PlaneSeg>	plane_params;
        vector<CylinderSeg>	cylinder_params;
	_cape->process(_organized_cloud, nplanes, ncylinders,
		       _label, plane_params, cylinder_params);

	colorize();

	_seg_image.encoding = sensor_msgs::image_encodings::RGB8;
	_seg_image.header   = depth_msg->header;
	_image_pub.publish(_seg_image.toImageMsg());
    }
    catch (const std::exception& e)
    {
	ROS_WARN_STREAM(e.what());
    }
}

void
Detector::cloud_cb(const cloud_p& cloud_msg)
{
    try
    {
	using namespace	aist_utility;

	set_sizes(cloud_msg->height, cloud_msg->width);
	
	pointcloud_to_points<float>(*cloud_msg, _cloud.begin(), meters<float>);
	organize_cloud();
	_label = 0;
	
	int			nplanes, ncylinders;
        vector<PlaneSeg>	plane_params;
        vector<CylinderSeg>	cylinder_params;
	_cape->process(_organized_cloud, nplanes, ncylinders,
		       _label, plane_params, cylinder_params);

	colorize();
	
	_seg_image.encoding = sensor_msgs::image_encodings::RGB8;
	_seg_image.header   = cloud_msg->header;
	_image_pub.publish(_seg_image.toImageMsg());
    }
    catch (const std::exception& e)
    {
	ROS_WARN_STREAM(e.what());
    }
}
    
template <class T> void
Detector::set_cape_parameter(T Detector::* field, T value)
{
    this->*field = value;

    set_sizes(_cloud.height(), _cloud.width());
}

void
Detector::set_sizes(size_t height, size_t width)
{
  // Setup input cloud.
    if (height != _cloud.height() || width != _cloud.width())
	_cloud.resize(height, width);
    
  // Setup cell map.
    _cell_map = cv::Mat_<int>(_cloud.height(), _cloud.width());
    const auto	h_ncells  = _cell_map.cols / _patch_size;
    const auto	cell_area = _patch_size * _patch_size;
    for (size_t v = 0; v < _cell_map.rows; ++v)
    {
	const auto	cell_v  = v / _patch_size;
	const auto	local_v = v % _patch_size;

	for (size_t u = 0; u < _cell_map.cols; ++u)
	{
	    const auto	cell_u  = u / _patch_size;
	    const auto	local_u = u % _patch_size;
	    _cell_map(v, u) = cell_area*(h_ncells*cell_v + cell_u)
			    + _patch_size*local_v + local_u;
	}
    }

  // Setup organized cloud.
    if (_organized_cloud.rows() != _cloud.height() * _cloud.width() ||
	_organized_cloud.cols() != 3)
	_organized_cloud.resize(_cloud.height() * _cloud.width(), 3);
    
  // Setup plane detector.
    _cape.reset(new CAPE(_cloud.height(), _cloud.width(),
			 _patch_size, _patch_size,
			 _detect_cylinder, _cos_angle_max, _max_merge_dist));

  // Setup segmentation image.
    if (_label.rows != _cloud.height() ||
	_label.cols != _cloud.width())
	_label = cv::Mat_<uint8_t>(_cloud.height(), _cloud.width(),
				   uint8_t(0));

  // Setup segment<ation image.
    if (_seg_image.image.rows != _cloud.height() ||
	_seg_image.image.cols != _cloud.width())
	_seg_image.image = cv::Mat(_cloud.height(), _cloud.width(), CV_8UC3);
}
    
void
Detector::organize_cloud()
{
    const auto	mxn  = _cloud.height() * _cloud.width();
    const auto	mxn2 = 2*mxn;
    const auto	q    = _organized_cloud.data();
    
    for(size_t v = 0; v < _cloud.height(); ++v)
    {
        const auto	row = _cell_map[v];

	for(size_t u = 0; u < _cloud.width(); ++u)
	{
            const int	id = row[u];
	    const auto	p  = _cloud.get(v, u);
	    if (std::isnan(p(2)))
	    {
		*(q	   + id) = 0;
		*(q + mxn  + id) = 0;
		*(q + mxn2 + id) = 0;
	    }
	    else
	    {
		*(q	   + id) = p(0);
		*(q + mxn  + id) = p(1);
		*(q + mxn2 + id) = p(2);
	    }
        }
    }
}

void
Detector::colorize()
{
    for (size_t v = 0; v < _label.rows; ++v)
    {
	const auto	p = _label[v];
	const auto	q = _seg_image.image.ptr<cv::Vec3b>(v);
	
	for (size_t u = 0; u < _label.cols; ++u)
	    q[u] = _colormap[p[u]];
    }
}
    
}	// namespace cape_ros
