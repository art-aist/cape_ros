/*
 * Copyright 2018 Pedro Proenza <p.proenca@surrey.ac.uk> (University of Surrey)
 *
 */

#include "Histogram.h"

Histogram::Histogram(int nr_bins_per_coord)
    :_nr_bins_per_coord(nr_bins_per_coord),
     _H(_nr_bins_per_coord * _nr_bins_per_coord, 0)
{
}

void
Histogram::initHistogram(const Eigen::MatrixXd& P, const vector<bool>& Flags)
{
    _B.assign(P.rows(), -1); 

  // Fill structures
    for (int i = 0; i < _B.size(); i++)
	if (Flags[i])
	{
	  // Set limits
	  // Polar angle [0 pi]
	    constexpr double	min_X(0), max_X(3.14);
	  // Azimuth angle [-pi pi]
	    constexpr double	min_Y(-3.14), max_Y(3.14);
	    
	    const int	X_q = (_nr_bins_per_coord - 1)*(P(i, 0) - min_X)
			    / (max_X - min_X);

	  // Dealing with degeneracy
	    const int	Y_q = (X_q > 0 ?
			       (_nr_bins_per_coord - 1)*(P(i, 1) - min_Y) /
			       (max_Y - min_Y) : 0);

	    const int	bin  = Y_q*_nr_bins_per_coord + X_q;
	    if (bin >= _B.size())
	    {
		std::cerr << "bin=" << bin << " while _B.size()=" << _B.size()
			  << std::endl;
		std::cerr << "P(i, 0)=" << P(i, 0) << ", P(i, 1)=" << P(i, 1)
			  << std::endl;
	    }
	    _B[i] = bin;
	    _H[bin]++;
	}
}

vector<int>
Histogram::getPointsFromMostFrequentBin()
{
    vector<int> point_ids;

    int most_frequent_bin = -1;
    int max_nr_occurrences = 0;

    for (int i = 0; i < _H.size(); i++)
	if (_H[i] > max_nr_occurrences)
	{
	    most_frequent_bin  = i;
	    max_nr_occurrences = _H[i];
	}

    if (max_nr_occurrences > 0)
	for (int i = 0; i < _B.size(); i++)
	    if (_B[i] == most_frequent_bin)
		point_ids.push_back(i);
    
    return point_ids;
}

void
Histogram::removePoint(int point_id)
{
    _H[_B[point_id]]--;
    _B[point_id] = -1;
}
