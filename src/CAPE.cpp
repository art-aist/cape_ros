/*
 * Copyright 2018 Pedro Proenza <p.proenca@surrey.ac.uk> (University of Surrey)
 *
 */

#include "CAPE.h"

CAPE::CAPE(int depth_height, int depth_width,
	   int cell_width, int cell_height, bool cylinder_detection,
	   float min_cos_angle_4_merge, float max_merge_dist)
    :_depth_height(depth_height),
     _depth_width(depth_width),
     _cell_width(cell_width),
     _cell_height(cell_height),
     _max_merge_dist(max_merge_dist),
     _min_cos_angle_4_merge(min_cos_angle_4_merge),
     _cylinder_detection(cylinder_detection)
{
    const int nr_horizontal_cells = _depth_width/_cell_width;
    const int nr_vertical_cells	  = _depth_height/_cell_height;
    _grid_plane_seg_map
	= cv::Mat_<int>(nr_vertical_cells, nr_horizontal_cells, 0);
    _grid_plane_seg_map_eroded
	= cv::Mat_<uchar>(nr_vertical_cells, nr_horizontal_cells, uchar(0));
    _grid_cylinder_seg_map
	= cv::Mat_<int>(nr_vertical_cells, nr_horizontal_cells, 0);
    _grid_cylinder_seg_map_eroded
	= cv::Mat_<uchar>(nr_vertical_cells, nr_horizontal_cells, uchar(0));

    _mask = cv::Mat(nr_vertical_cells,nr_horizontal_cells,CV_8U);
    _mask_eroded = cv::Mat(nr_vertical_cells,nr_horizontal_cells,CV_8U);
    _mask_dilated = cv::Mat(nr_vertical_cells,nr_horizontal_cells,CV_8U);
    _mask_diff = cv::Mat(nr_vertical_cells,nr_horizontal_cells,CV_8U);
	
    const int nr_total_cells  = nr_vertical_cells * nr_horizontal_cells;
    const int nr_pts_per_cell = _cell_width * _cell_height;
	
    for(int i = 0; i < nr_total_cells; i++)
	_Grid.push_back(NULL);

    _distances_stacked
	= (float*)malloc(_depth_height*_depth_width*sizeof(float));
    _distances_cell_stacked = Eigen::ArrayXf::Zero(nr_pts_per_cell,1);
    _seg_map_stacked
	= (unsigned char*)malloc(_depth_height*_depth_width*sizeof(unsigned char));
    _activation_map  = (bool*)malloc(nr_total_cells*sizeof(bool));
    _unassigned_mask = (bool*)malloc(nr_total_cells*sizeof(bool));

    _mask_square_kernel = cv::Mat::ones(3,3,CV_8U);

    _mask_cross_kernel = cv::Mat::ones(3,3,CV_8U);
    _mask_cross_kernel.at<uchar>(0,0) = 0;
    _mask_cross_kernel.at<uchar>(2,2) = 0;
    _mask_cross_kernel.at<uchar>(0,2) = 0;
    _mask_cross_kernel.at<uchar>(2,0) = 0;
}

CAPE::~CAPE()
{
    _Grid.clear();
}

void
CAPE::process(const Eigen::MatrixXf& cloud_array,
	      int& nr_planes_final, int& nr_cylinders_final,
	      cv::Mat& seg_out, vector<PlaneSeg>& plane_segments_final,
	      vector<CylinderSeg>& cylinder_segments_final)
{
    const int nr_horizontal_cells  = _depth_width/_cell_width;
    const int nr_vertical_cells    = _depth_height/_cell_height;
    const int nr_total_cells	   = nr_vertical_cells*nr_horizontal_cells;
    const int nr_pts_per_cell	   = _cell_width*_cell_height;
    const int cylinder_code_offset = 50;

    _grid_plane_seg_map		  = 0;
    _grid_plane_seg_map_eroded	  = 0;
    _grid_cylinder_seg_map	  = 0;
    _grid_cylinder_seg_map_eroded = 0;
    std::memset(_seg_map_stacked, (uchar)0,
		_depth_height*_depth_width*sizeof(unsigned char));
    std::memset(_distances_stacked, 100,
		_depth_height*_depth_width*sizeof(float)); /* = to really high float*/

  /*--------------- Planar cell fitting  ---------------------------------*/
    std::vector<float>	cell_distance_tols(nr_total_cells,0);
    int			stacked_cell_id = 0;
    const float sin_cos_angle_4_merge = sqrt(1-pow(_min_cos_angle_4_merge,2));
    for (int cell_r = 0; cell_r < nr_vertical_cells; cell_r++)
    {
	for (int cell_c = 0; cell_c < nr_horizontal_cells; cell_c++)
	{
	    _Grid[stacked_cell_id] = new PlaneSeg(cloud_array, stacked_cell_id,
						  nr_pts_per_cell,
						  _cell_width);
	    if (_Grid[stacked_cell_id]->planar)
	    {
		const float cell_diameter
				= (cloud_array.block(
				       stacked_cell_id*nr_pts_per_cell +
				       nr_pts_per_cell - 1,
				       0, 1, 3) -
				   cloud_array.block(
				       stacked_cell_id*nr_pts_per_cell,
				       0, 1, 3)).norm();
	      // Add truncated distance
		cell_distance_tols[stacked_cell_id]
		    = pow(min(max(cell_diameter*sin_cos_angle_4_merge,
				  20.0f), _max_merge_dist), 2);
	    }

	    stacked_cell_id++;
	}
    }

  /*--- Initialize histogram -----------------------------------*/
  //double t3 = cv::getTickCount();
  // Spherical coordinates
    Eigen::MatrixXd	C(nr_total_cells, 2);
    vector<bool>	planar_flags(nr_total_cells, false);
    vector<float>	scores_stacked(nr_total_cells, 0.0);
    int			nr_remaining_planar_cells = 0;
    for (int cell_id = 0; cell_id < nr_total_cells; cell_id++)
	if (_Grid[cell_id]->planar)
	{
	    const double	nx = _Grid[cell_id]->normal[0];
	    const double	ny = _Grid[cell_id]->normal[1];
	    const double	nz = _Grid[cell_id]->normal[2];
	    const double	n_proj_norm = sqrt(nx*nx + ny*ny);
	    C(cell_id, 0)	    = acos(-nz);
	  //C(cell_id, 1)	    = atan2(nx/n_proj_norm, ny/n_proj_norm); 
	    C(cell_id, 1)	    = atan2(nx, ny);
	    planar_flags[cell_id]   = true;
	    scores_stacked[cell_id] = _Grid[cell_id]->score;
	    nr_remaining_planar_cells++;
	}

    Histogram	H(20);
    H.initHistogram(C, planar_flags);

  // Initialization for cell-wise region growing and model fitting
    vector<PlaneSeg>		plane_segments;
    vector<CylinderSeg>		cylinder_segments;
    bool			stop = false;
    int				nr_cylinders = 0;
    vector<pair<int,int> >	cylinder2region_map;

    for (int cell_id=0; cell_id<nr_total_cells; cell_id++)
	_unassigned_mask[cell_id] = planar_flags[cell_id];

  /*----- Cell-wise region growing with embedded model fitting --------------*/
    while(nr_remaining_planar_cells>0)
    {  
      // 1. Seeding
      // Pick up seed candidates
	vector<int>	seed_candidates = H.getPointsFromMostFrequentBin();

      // Checkpoint 1
	if (seed_candidates.size() < 5)
	    break;

      // Select seed based on score
	int	seed_id;
	float	min_MSE = INT_MAX;
	for(int i = 0; i < seed_candidates.size(); ++i)
	{
	    const int	seed_candidate = seed_candidates[i];

	    if (_Grid[seed_candidate]->MSE < min_MSE)
	    {
		seed_id =  seed_candidate;
		min_MSE = _Grid[i]->MSE;
	    }
	}

	PlaneSeg new_ps = *_Grid[seed_id];

      // 2. Actual seed cell growing
	const int	y = seed_id/nr_horizontal_cells;
	const int	x = seed_id%nr_horizontal_cells;
	const double*	seed_n = new_ps.normal;
	const double	seed_d = new_ps.d;
	memset(_activation_map, false, sizeof(bool)*nr_total_cells);
	RegionGrowing(nr_horizontal_cells, nr_vertical_cells,
		      _unassigned_mask, _activation_map,
		      _Grid, cell_distance_tols, x, y, seed_n, seed_d);

      // 3. Merge activated cells & remove them from histogram and list of remaining cells
	int	nr_cells_activated = 0;
	for(int i = 0; i < nr_total_cells; ++i)
	    if (_activation_map[i])
	    {
		new_ps.expandSegment(_Grid[i]);
		nr_cells_activated++;
		H.removePoint(i);
		_unassigned_mask[i] = false;
		nr_remaining_planar_cells--;
	    }

      // Checkpoint 2
	if (nr_cells_activated < 4)
	    continue;

	new_ps.fitPlane();

      // 4. Model fitting
	if (new_ps.score>100)
	{
	  // It is a plane
	    plane_segments.push_back(new_ps);
	    const int	nr_curr_planes = plane_segments.size();
	  // Mark cells
	    int		i=0;
	    for(int r = 0; r < nr_vertical_cells; ++r)
	    {
		int* const	row = _grid_plane_seg_map.ptr<int>(r);

		for(int c=0; c<nr_horizontal_cells; c++)
		{
		    if (_activation_map[i])
			row[c] = nr_curr_planes;
		    i++;
		}
	    }
	}
	else if (_cylinder_detection && nr_cells_activated>5)
	{
	  // It is an extrusion
	    CylinderSeg cy(_Grid, _activation_map, nr_cells_activated);
	    cylinder_segments.push_back(cy);
	  // Fit planes to subsegments
	    for (int seg_id=0; seg_id<cy.nr_segments; seg_id++)
	    {
		new_ps.clearPoints();
		for (int c = 0; c < nr_cells_activated; c++)
		    if (cy.inliers[seg_id](c))
			new_ps.expandSegment(_Grid[cy.local2global_map[c]]);

		new_ps.fitPlane();
	      // Model selection based on MSE
		if(new_ps.MSE < cy.MSEs[seg_id])
		{
		    plane_segments.push_back(new_ps);
		    const int nr_curr_planes = plane_segments.size();
		    for (int c = 0; c < nr_cells_activated; c++)
			if (cy.inliers[seg_id](c))
			{
			    const int cell_id = cy.local2global_map[c];
			    _grid_plane_seg_map.at<int>(
				cell_id/nr_horizontal_cells,
				cell_id%nr_horizontal_cells) = nr_curr_planes;
			}

		    cy.cylindrical_mask[seg_id] = false;
		}
		else
		{
		    nr_cylinders++;
		    cylinder2region_map.push_back(
			make_pair(cylinder_segments.size()-1,seg_id));
		    
		    for (int c = 0; c < nr_cells_activated; c++)
			if (cy.inliers[seg_id](c))
			{
			    const int cell_id = cy.local2global_map[c];
			    _grid_cylinder_seg_map.at<int>(
				cell_id/nr_horizontal_cells,
				cell_id%nr_horizontal_cells) = nr_cylinders;
			}

		    cy.cylindrical_mask[seg_id] = true;
		}
	    }
	}
    }

  /*----------------- Plane merging ------------------------------------*/
    const int	nr_planes =  plane_segments.size();
    MatrixXb	planes_association_matrix= MatrixXb::Zero(nr_planes,nr_planes);
    getConnectedComponents(_grid_plane_seg_map, planes_association_matrix);

    vector<int>	plane_merge_labels;
    for(int i=0; i<nr_planes; i++)
	plane_merge_labels.push_back(i);

  // Connect compatible planes
    for(int r = 0; r < planes_association_matrix.rows(); r++)
    {
	const int	plane_id = plane_merge_labels[r];
	bool		plane_expanded = false;
	
	for (int c = r + 1; c < planes_association_matrix.cols(); c++)
	    if(planes_association_matrix(r,c))
	    {
		const double cos_angle
		    = plane_segments[
			plane_id].normal[0]*plane_segments[c].normal[0]
 		      + plane_segments[
			  plane_id].normal[1]*plane_segments[c].normal[1]
		      + plane_segments[
			  plane_id].normal[2]*plane_segments[c].normal[2];
		const double distance
		    = pow(plane_segments[r].normal[0]*plane_segments[c].mean[0]
			  + plane_segments[plane_id].normal[1]*plane_segments[c].mean[1]  
			  + plane_segments[plane_id].normal[2]*plane_segments[c].mean[2]
			  + plane_segments[plane_id].d,2);

		if (cos_angle > _min_cos_angle_4_merge &&
		    distance < _max_merge_dist)
		{
		    plane_segments[plane_id].expandSegment(&plane_segments[c]);
		    plane_merge_labels[c] = plane_id;
		    plane_expanded = true;
		}
		else
		{
		    planes_association_matrix(r,c) = false;
		}
	    }

	if(plane_expanded)
	    plane_segments[plane_id].fitPlane();
    }

  /*------------ Refine plane boundaries -------------------------------*/
  //vector<PlaneSeg> plane_segments_joint;
    for (int i=0; i<nr_planes;i++)
    {
	if (i != plane_merge_labels[i])
	    continue;

      // Build mask w/ merged segments
	_mask = cv::Scalar(0);
	for (int j = i; j < nr_planes; j++)
	    if (plane_merge_labels[j]==plane_merge_labels[i])
		_mask.setTo(1, _grid_plane_seg_map==j+1);

      // Erode with cross to obtain borders and check support
	cv::erode(_mask,_mask_eroded,_mask_cross_kernel);
	double min, max;
	cv::minMaxLoc(_mask_eroded, &min, &max);

      // If completely eroded ignore plane
	if (max==0)
	    continue;

        plane_segments_final.push_back(plane_segments[i]);

      // Dilate to obtain borders
	cv::dilate(_mask,_mask_dilated,_mask_square_kernel);
	_mask_diff = _mask_dilated-_mask_eroded;

	int stacked_cell_id = 0;
        const uchar plane_nr = (unsigned char)plane_segments_final.size();
	const float nx = (float)plane_segments[i].normal[0];
	const float ny = (float)plane_segments[i].normal[1];
	const float nz = (float)plane_segments[i].normal[2];
	const float d = (float)plane_segments[i].d;

	_grid_plane_seg_map_eroded.setTo(plane_nr, _mask_eroded > 0);

      // Cell refinement
	for (int cell_r = 0; cell_r < nr_vertical_cells; cell_r++)
	{
	    const unsigned char* const row_ptr = _mask_diff.ptr<uchar>(cell_r);
	    
	    for (int cell_c = 0; cell_c < nr_horizontal_cells; cell_c++)
	    {
		const int offset = stacked_cell_id*nr_pts_per_cell;
		const int next_offset = offset+nr_pts_per_cell;

		if (row_ptr[cell_c] > 0)
		{
		    const float max_dist = 9*plane_segments[i].MSE;
		  // Compute distance block
		    _distances_cell_stacked
			= cloud_array.block(
			    offset,0,nr_pts_per_cell,1).array()*nx
			+ cloud_array.block(
			    offset,1,nr_pts_per_cell,1).array()*ny
			+ cloud_array.block(
			    offset,2,nr_pts_per_cell,1).array()*nz
			+ d;

		  // Assign pixels
		    int j = 0;
		    for (int pt = offset; pt < next_offset; j++, pt++)
		    {
			const float dist = pow(_distances_cell_stacked(j), 2);
			
			if(dist < max_dist && dist<_distances_stacked[pt])
			{ 
			    _distances_stacked[pt] = dist;
			    _seg_map_stacked[pt] = plane_nr;
			}
		    }
		}

		stacked_cell_id++;
	    }
	}
    }
    nr_planes_final = plane_segments_final.size();

  /*------- Refine cylinder boundaries -----------------------------*/
    nr_cylinders_final = 0;
    if (_cylinder_detection)
    {
	Eigen::Vector3f point;
	float dist;

	for (int i = 0; i < nr_cylinders; i++)
	{
	    const int cylinder_nr = i+1;
	    const int reg_id = cylinder2region_map[i].first;
	    const int sub_reg_id = cylinder2region_map[i].second;

	  // Build mask
	    _mask = cv::Scalar(0);
	    _mask.setTo(1, _grid_cylinder_seg_map==cylinder_nr);

	  // Erode to obtain borders
	    cv::erode(_mask,_mask_eroded,_mask_cross_kernel);
	    double min, max;
	    cv::minMaxLoc(_mask_eroded, &min, &max);

	  // If completely eroded ignore cylinder
	    if (max==0)
		continue;

	    nr_cylinders_final++;

	  // Dilate to obtain borders
	    cv::dilate(_mask,_mask_dilated,_mask_square_kernel);
	    _mask_diff = _mask_dilated-_mask_eroded;

	    int stacked_cell_id = 0;

	    _grid_cylinder_seg_map_eroded
		.setTo((unsigned char)cylinder_code_offset+nr_cylinders_final,
		       _mask_eroded > 0);

	  // Get variables needed for point-surface distance computation
	    const Eigen::Vector3f P2 = cylinder_segments[reg_id].P2[sub_reg_id];
	    const Eigen::Vector3f P1P2 = P2
				 - cylinder_segments[reg_id].P1[sub_reg_id];
	    const double P1P2_norm	 = cylinder_segments[reg_id].P1P2_norm[sub_reg_id];
	    const double radius	 = cylinder_segments[reg_id].radii[sub_reg_id];

	  // Cell refinement
	    for (int cell_r = 0; cell_r < nr_vertical_cells; cell_r++)
	    {
		const unsigned char* const row_ptr = _mask_diff.ptr<uchar>(cell_r);
		for (int cell_c = 0; cell_c < nr_horizontal_cells; cell_c++)
		{
		    const int offset = stacked_cell_id*nr_pts_per_cell;
		    const int next_offset = offset+nr_pts_per_cell;

		    if(row_ptr[cell_c]>0)
		    {
			const float max_dist
			    = 9*cylinder_segments[reg_id].MSEs[sub_reg_id];
		      // Update cells
			int j=0;
			for(int pt = offset; pt<next_offset;j++,pt++)
			{
			    point(2) = cloud_array(pt,2);
			    
			    if(point(2)>0)
			    {
				point(0) = cloud_array(pt,0);
				point(1) = cloud_array(pt,1);
				dist = P1P2.cross(point-P2).norm()/P1P2_norm
				     - radius;
				dist *= dist;

				if(dist<max_dist && dist<_distances_stacked[pt])
				{ 
				    _distances_stacked[pt] = dist;
				    _seg_map_stacked[pt] = cylinder_code_offset
							+ nr_cylinders_final;
				}
			    }
			}
		    }

		    stacked_cell_id++;
		}
	    }
	}
    }

  /*------------------------- Copying and rearranging segment data ------------------------*/

    uchar *row_ptr, *stack_ptr;
    uchar *grid_plane_eroded_row_ptr, *grid_cylinder_eroded_row_ptr;

  // Copy inlier list to matrix form
    for (int cell_r = 0; cell_r < nr_vertical_cells; cell_r++)
    {
	row_ptr = seg_out.ptr<uchar>(cell_r);
	grid_plane_eroded_row_ptr
	    = _grid_plane_seg_map_eroded.ptr<uchar>(cell_r);
	grid_cylinder_eroded_row_ptr
	    = _grid_cylinder_seg_map_eroded.ptr<uchar>(cell_r);

	const int r_offset = cell_r*_cell_height;
	const int r_limit = r_offset+_cell_height;

	for (int cell_c=0; cell_c<nr_horizontal_cells; cell_c++)
	{
	    const int c_offset = cell_c*_cell_width;
	    const int c_limit = c_offset+_cell_width;

	    if (grid_plane_eroded_row_ptr[cell_c]>0)
	    {
	      // Set rectangle equal to assigned cell
		seg_out(cv::Rect(c_offset, r_offset,
				 _cell_width, _cell_height))
		    .setTo(grid_plane_eroded_row_ptr[cell_c]);
	    }
	    else
	    {
		if(grid_cylinder_eroded_row_ptr[cell_c]>0)
		{
		  // Set rectangle equal to assigned cell
		    seg_out(cv::Rect(c_offset, r_offset,
				     _cell_width, _cell_height))
			.setTo(grid_cylinder_eroded_row_ptr[cell_c]);
		}
		else
		{
		  // Set cell pixels one by one
		    stack_ptr = &_seg_map_stacked[nr_pts_per_cell*cell_r*
						 nr_horizontal_cells +
						 nr_pts_per_cell*cell_c];
		    for(int r=r_offset;r<r_limit;r++)
		    {
			row_ptr = seg_out.ptr<uchar>(r);

			for(int c=c_offset;c<c_limit;c++)
			{
			    if(*stack_ptr>0)
				row_ptr[c] = *stack_ptr;
			    stack_ptr++;
			}
		    }
		}
	    }
	}
    }

    for(int i=0;i<nr_cylinders;i++)
    {
        int reg_id = cylinder2region_map[i].first;

	if (reg_id>-1)
	{
            const int	sub_reg_id = cylinder2region_map[i].second;
            CylinderSeg	cy;
            cy.radii.push_back(cylinder_segments[reg_id].radii[sub_reg_id]);
            cy.centers.push_back(
		cylinder_segments[reg_id].centers[sub_reg_id]);
            copy(cylinder_segments[reg_id].axis,
		 cylinder_segments[reg_id].axis+3, cy.axis);

	    cout << cylinder_segments[reg_id].axis[2] << endl;

	    cylinder_segments_final.push_back(cy);
        }
    }


  // Cleaning data
    stacked_cell_id=0;
    for (int cell_r = 0; cell_r < nr_vertical_cells; cell_r++)
	for (int cell_c = 0; cell_c < nr_horizontal_cells; cell_c++)
	{
	    delete _Grid[stacked_cell_id];
	    stacked_cell_id++;
	}

  //_Grid.clear();
}

void
CAPE::getConnectedComponents(cv::Mat& segment_map,
			     MatrixXb& planes_association_matrix)
{
    const int nr_rows_2_scan = segment_map.rows-1;
    const int nr_cols_2_scan = segment_map.cols-1;

    for(int r = 0; r < nr_rows_2_scan; r++)
    {
	const int* const row = segment_map.ptr<int>(r);
	const int* const row_below = segment_map.ptr<int>(r+1);
	for (int c = 0; c < nr_cols_2_scan; c++)
	{
	    const int px_val = row[c];
	    if (px_val > 0)
	    {
		if (row[c+1]>0 && px_val != row[c+1])
		    planes_association_matrix(px_val-1,row[c+1]-1) = true;
		if (row_below[c]>0 && px_val != row_below[c])
		    planes_association_matrix(px_val-1,row_below[c]-1) = true;
	    }
	}
    }

    for (int r = 0; r < planes_association_matrix.rows(); r++)
	for (int c = r + 1; c < planes_association_matrix.cols(); c++)
	    planes_association_matrix(r,c) = planes_association_matrix(r,c)
					  || planes_association_matrix(c,r);
}

// Recursive implementation
// TODO: Test instead iterative implementation
void
CAPE::RegionGrowing(unsigned short width, unsigned short height,
		    const bool* input, bool* output,
		    const vector<PlaneSeg*>& _Grid,
		    const vector<float>& cell_dist_tols,
		    unsigned short x, unsigned short y,
		    const double* normal_1, double d)
{
    const int index = x + width*y;

  // If pixel is not part of a component or has already been labelled
    if (input[index]==false || output[index]==true)
	return;

    const double* const	normal_2 = _Grid[index]->normal;
    const double* const	m = _Grid[index]->mean;
    const double	d_2 = _Grid[index]->d;

    if (normal_1[0]*normal_2[0] + normal_1[1]*normal_2[1] +
	normal_1[2]*normal_2[2] < _min_cos_angle_4_merge || 
	pow(normal_1[0]*m[0] +
	    normal_1[1]*m[1] + normal_1[2]*m[2] + d,2)>cell_dist_tols[index])//_max_merge_dist
	return;
    output[index] = true;

  // Now label the 4 neighbours:
    if (x > 0)
	RegionGrowing(width, height, input, output, _Grid, cell_dist_tols,
		      x-1, y, normal_2, d_2);   // left  pixel
    if (x < width-1)
	RegionGrowing(width, height, input, output, _Grid, cell_dist_tols,
		      x+1, y, normal_2, d_2);  // right pixel
    if (y > 0)
	RegionGrowing(width, height, input, output, _Grid, cell_dist_tols,
		      x, y-1, normal_2, d_2);   // upper pixel 
    if (y < height-1)
	RegionGrowing(width, height, input, output, _Grid, cell_dist_tols,
		      x, y+1, normal_2, d_2);   // lower pixel
}
