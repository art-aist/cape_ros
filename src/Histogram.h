/*
 * Copyright 2018 Pedro Proenza <p.proenca@surrey.ac.uk> (University of Surrey)
 *
 */

#pragma once
#include <Eigen/Dense>
#include <vector>
#include <iostream>

using namespace std;
#define DMAX	std::numeric_limits<float>::max()
#define DMIN	std::numeric_limits<float>::min()

class Histogram
{
  public:
		Histogram(int nr_bins_per_coord)			;

    void	initHistogram(const Eigen::MatrixXd & Points,
			      const vector<bool> & Flags)		;
    vector<int>	getPointsFromMostFrequentBin()				;
    void	removePoint(int point_id)				;

  private:
    const int	_nr_bins_per_coord;
    vector<int>	_H;
    vector<int>	_B;
};

