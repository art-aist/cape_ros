/*
 * Copyright 2018 Pedro Proenza <p.proenca@surrey.ac.uk> (University of Surrey)
 *
 */
#pragma once
#include <math.h>
#include <iostream>
#include <iterator>
#include <algorithm>
#include "PlaneSeg.h"
#include "CylinderSeg.h"
#include "Histogram.h"
#include <opencv2/opencv.hpp>
#include <Eigen/Dense>

typedef Eigen::Matrix<bool,Eigen::Dynamic,Eigen::Dynamic>  MatrixXb;

class CAPE
{
  public:
		CAPE(int depth_height, int depth_width,
		     int cell_width, int cell_height,
		     bool cylinder_detection,
		     float min_cos_angle_4_merge = 0.97814,
		     float max_merge_dist = 900)			;
		~CAPE()							;
    
    void	process(const Eigen::MatrixXf& cloud_array, int& nr_planes,
			int& nr_cylinders, cv::Mat& seg_output,
			vector<PlaneSeg>& plane_segments_final,
			vector<CylinderSeg>& cylinder_segments_final)	;
    void	RegionGrowing(unsigned short width,
			      unsigned short height,
			      const bool* input, bool* output,
			      const vector<PlaneSeg*>& Grid,
			      const vector<float>& cell_dist_tols,
			      unsigned short x, unsigned short y,
			      const double* normal, double d)		;
    void	getConnectedComponents(cv::Mat& segment_map,
				       MatrixXb& planes_association_matrix);

  private:
    int				_cell_width;
    int				_cell_height;
    int				_depth_height;
    int				_depth_width;
    float			_max_merge_dist;
    float			_min_cos_angle_4_merge;
    bool			_cylinder_detection;
    std::vector<cv::Vec3b>	_color_code;
    std::vector<PlaneSeg*>	_Grid;
    cv::Mat_<int>		_grid_plane_seg_map;
    cv::Mat_<uchar>		_grid_plane_seg_map_eroded;
    cv::Mat_<int>		_grid_cylinder_seg_map;
    cv::Mat_<uchar>		_grid_cylinder_seg_map_eroded;
    cv::Mat			_mask;
    cv::Mat			_mask_eroded;
    cv::Mat			_mask_square_eroded;
    cv::Mat			_mask_dilated;
    cv::Mat			_mask_diff;
    cv::Mat			_mask_square_kernel;
    cv::Mat			_mask_cross_kernel;
    float*			_distances_stacked;
    Eigen::ArrayXf		_distances_cell_stacked;
    unsigned char*		_seg_map_stacked;
    bool*			_activation_map;
    bool*			_unassigned_mask;
};

