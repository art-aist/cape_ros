/*
 * Copyright 2018 Pedro Proenza <p.proenca@surrey.ac.uk> (University of Surrey)
 *
 */

#include "PlaneSeg.h"

PlaneSeg::PlaneSeg(const Eigen::MatrixXf& cloud_array,
		   int cell_id, int nr_pts_per_cell, int cell_width)
    :_nr_pts(0),
     _min_nr_pts(nr_pts_per_cell/2),
     _x_acc(0), _y_acc(0), _z_acc(0),
     _xx_acc(0), _yy_acc(0), _zz_acc(0), _xy_acc(0), _xz_acc(0), _yz_acc(0),
     planar(true)
{
    const int		offset	    = cell_id * nr_pts_per_cell;
    const int		cell_height = nr_pts_per_cell/cell_width;
    int			nan_counter = 0;
    const double	max_diff    = 100;
    const int		max_pt_id   = offset+nr_pts_per_cell;

    const Eigen::MatrixXf Z_matrix = cloud_array.block(offset, 2,
						       nr_pts_per_cell, 1);

  // Check nr of missing points
    _nr_pts = (Z_matrix.array() > 0).count();

    if (_nr_pts < _min_nr_pts)
    {
	planar = false;
	return;
    }

    Eigen::MatrixXf X_matrix = cloud_array.block(offset, 0, nr_pts_per_cell, 1);
    Eigen::MatrixXf Y_matrix = cloud_array.block(offset, 1, nr_pts_per_cell, 1);

  // Check for discontinuities using cross search
    int jumps_counter = 0;
    int i = cell_width*(cell_height/2);
    int j = i + cell_width;
    float z_last(max(Z_matrix(i), Z_matrix(i+1))); /* handles missing pixels on the borders*/
    i++;
  // Scan horizontally through the middle
    while (i < j)
    {
	float z = Z_matrix(i);
	if (z > 0 && abs(z - z_last) < max_diff)
	    z_last = z;
	else if (z > 0)
	    jumps_counter++;
	i++;
    }

    if (jumps_counter>1)
    {
	planar = false;
	return;
    }

  // Scan vertically through the middle
    i = cell_width/2;
    j = nr_pts_per_cell - i;
    z_last = max(Z_matrix(i), Z_matrix(i + cell_width));  /* handles missing pixels on the borders*/
    i = i + cell_width;
    jumps_counter = 0;
    while (i < j)
    {
	float z = Z_matrix(i);
	if(z > 0 && abs(z - z_last) < max_diff)
	    z_last = z;
	else if (z > 0)
	    jumps_counter++;
	i += cell_width;
    }

    if (jumps_counter > 1)
    {
	planar = false;
	return;
    }

    _x_acc = X_matrix.sum();
    _y_acc = Y_matrix.sum();
    _z_acc = Z_matrix.sum();
    _xx_acc = (X_matrix.array()*X_matrix.array()).sum();
    _yy_acc = (Y_matrix.array()*Y_matrix.array()).sum();
    _zz_acc = (Z_matrix.array()*Z_matrix.array()).sum();
    _xy_acc = (X_matrix.array()*Y_matrix.array()).sum();
    _xz_acc = (X_matrix.array()*Z_matrix.array()).sum();
    _yz_acc = (Y_matrix.array()*Z_matrix.array()).sum();

    if (planar)
    {
	fitPlane();

	if (MSE > pow(DEPTH_SIGMA_COEFF*mean[2]*mean[2] + DEPTH_SIGMA_MARGIN,
		      2))
	    planar = false;
    }
}

void
PlaneSeg::expandSegment(const PlaneSeg* plane_seg)
{
    _x_acc += plane_seg->_x_acc;
    _y_acc += plane_seg->_y_acc;
    _z_acc += plane_seg->_z_acc;

    _xx_acc += plane_seg->_xx_acc;
    _yy_acc += plane_seg->_yy_acc;
    _zz_acc += plane_seg->_zz_acc;
    _xy_acc += plane_seg->_xy_acc;
    _xz_acc += plane_seg->_xz_acc;
    _yz_acc += plane_seg->_yz_acc;

    _nr_pts += plane_seg->_nr_pts;
}

void
PlaneSeg::clearPoints()
{
    _x_acc = 0;
    _y_acc = 0;
    _z_acc = 0;

    _xx_acc = 0;
    _yy_acc = 0;
    _zz_acc = 0;
    _xy_acc = 0;
    _xz_acc = 0;
    _yz_acc = 0;

    _nr_pts = 0;
}

void
PlaneSeg::fitPlane()
{
    mean[0] = _x_acc/_nr_pts;
    mean[1] = _y_acc/_nr_pts;
    mean[2] = _z_acc/_nr_pts;

  // Expressing covariance as E[PP^t] + E[P]*E[P^T]
    double cov[3][3] = {{_xx_acc - _x_acc*_x_acc/_nr_pts,
			 _xy_acc - _x_acc*_y_acc/_nr_pts,
			 _xz_acc - _x_acc*_z_acc/_nr_pts},
			{0,
			 _yy_acc - _y_acc*_y_acc/_nr_pts,
			 _yz_acc - _y_acc*_z_acc/_nr_pts},
			{0,
			 0,
			 _zz_acc - _z_acc*_z_acc/_nr_pts}};
    cov[1][0] = cov[0][1];
    cov[2][0] = cov[0][2];
    cov[2][1] = cov[1][2];

  // This uses QR decomposition for symmetric matrices
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d>
			es(Eigen::Map<Eigen::Matrix3d>(cov[0], 3, 3));
    const Eigen::VectorXd	v = es.eigenvectors().col(0);

    d = -(v[0]*mean[0] + v[1]*mean[1] + v[2]*mean[2]);

  // Enforce normal orientation
    if (d > 0)
    {
	normal[0] = v[0];
	normal[1] = v[1];
	normal[2] = v[2];
    }
    else
    {
	normal[0] = -v[0];
	normal[1] = -v[1];
	normal[2] = -v[2];
	d = -d;
    } 

    MSE   = es.eigenvalues()[0]/_nr_pts;
    score = es.eigenvalues()[1]/es.eigenvalues()[0];
}
